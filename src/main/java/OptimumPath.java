package main.java;

import java.util.ArrayList;

/**
 * Created by Фирюза on 24.11.14.
 */
public class OptimumPath {

    private int numberOfVertices;
    private int[] vertices;
    private int[] vVertex;
    private int[] wVertex;
    private double[][] adjMatrix;
    private double[] forSorting;
    private ArrayList<Integer> aboveSet = new ArrayList<Integer>();
    private ArrayList<Integer> underSet = new ArrayList<Integer>();
    private Point[] points;
    private int[][] aboveSetEdges;
    private int[][] underSetEdges;

    public OptimumPath(int n) {
        this.numberOfVertices = n;
        this.vertices = new int[n];
        this.vVertex = new int[n];
        this.wVertex = new int[n];
        this.adjMatrix = new double[n][n];
        this.points = new Point[n];

        for (int i = 0; i < n; i++) {
            this.vVertex[i] = -1;
            this.wVertex[i] = -1;
        }
    }

    public void setValue(int i, int j, double value) {
        this.adjMatrix[i][j] = value;
    }

    public void setCoordinates(int i, double x, double y) {
        points[i].setX(x);
        points[i].setY(y);
    }

    public void FindOptimumPath() {
        this.divideOnSets();
        this.sortSets();

        this.setPathInAboveLocalSet();
        this.setPathInAboveSet();
    }

    private void divideOnSets() {
        for (int i = 1; i < this.numberOfVertices; i++) {
            if (this.points[0].getY() > this.points[i].getY()) {
                this.underSet.add(i);
            }
            else {
                this.aboveSet.add(i);
            }
        }
    }

    private void sortSets() {
        if (this.aboveSet.size() != 0) {
            this.sort("above");
        }
        if(this.underSet.size() != 0) {
            this.sort("under");
        }
    }

    private void setPathInAboveLocalSet() {
        int n = this.aboveSet.size() / 3;
        if (this.aboveSet.size() % 3 != 0) {
            n++;
        }
        this.aboveSetEdges = new int[n][2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 2; j++) {
                this.aboveSetEdges[i][j] = -1;
            }
        }
        this.recurFindPathAbove(0, 2, this.aboveSet.size(), 0);
    }

    private void setPathInAboveSet() {
        this.connectBlocks();
    }

    private void recurFindPathAbove(int begin, int end, int n, int numBlock) {
        if (begin >= n) {
            return;
        }
        if (end >= n) {
            end = n - 1;
        }
        this.findPathInBlockAbove(begin, end, numBlock);
        numBlock++;
        this.recurFindPathAbove(begin + 3, end + 3, n, numBlock);
    }

    private void findPathInBlockAbove(int begin, int end, int numBlock) {
        if (begin == end) {
            this.vVertex[this.aboveSet.get(begin)] = -1;
            this.wVertex[this.aboveSet.get(begin)] = -1;
            this.aboveSetEdges[numBlock][0] = this.aboveSet.get(begin);
            this.aboveSetEdges[numBlock][1] = -1;
        }
        else {
            if (end - begin < 3) {
                this.vVertex[this.aboveSet.get(begin)] = this.aboveSet.get(end);
                this.wVertex[this.aboveSet.get(begin)] = -1;
                this.vVertex[this.aboveSet.get(end)] = this.aboveSet.get(begin);
                this.wVertex[this.aboveSet.get(end)] = -1;
                this.aboveSetEdges[numBlock][0] = this.aboveSet.get(begin);
                this.aboveSetEdges[numBlock][1] = this.aboveSet.get(end);
            }
            else {
                double weight1, weight2, weight3;
                weight1 = this.adjMatrix[this.aboveSet.get(begin)][this.aboveSet.get(begin + 1)] +
                        this.adjMatrix[this.aboveSet.get(begin+1)][this.aboveSet.get(end)];

                weight2 = this.adjMatrix[this.aboveSet.get(begin)][this.aboveSet.get(end)] +
                        this.adjMatrix[this.aboveSet.get(end)][this.aboveSet.get(begin + 1)];

                weight3 = this.adjMatrix[this.aboveSet.get(begin + 1)][this.aboveSet.get(begin)] +
                        this.adjMatrix[this.aboveSet.get(begin)][this.aboveSet.get(end)];

                if (minAmongThree(weight1,weight2, weight3) == 1) {
                    this.vVertex[this.aboveSet.get(begin)] = this.aboveSet.get(begin + 1);
                    this.wVertex[this.aboveSet.get(begin)] = -1;
                    this.vVertex[this.aboveSet.get(begin + 1)] = this.aboveSet.get(end);
                    this.wVertex[this.aboveSet.get(begin + 1)] = this.aboveSet.get(begin);
                    this.vVertex[this.aboveSet.get(end)] = this.aboveSet.get(begin + 1);
                    this.wVertex[this.aboveSet.get(end)] = -1;
                    this.aboveSetEdges[numBlock][0] = this.aboveSet.get(begin);
                    this.aboveSetEdges[numBlock][1] = this.aboveSet.get(end);
                }
                else {
                    if (minAmongThree(weight1,weight2, weight3) == 2) {
                        this.vVertex[this.aboveSet.get(begin)] = this.aboveSet.get(end);
                        this.wVertex[this.aboveSet.get(begin)] = -1;
                        this.vVertex[this.aboveSet.get(end)] = this.aboveSet.get(begin + 1);
                        this.wVertex[this.aboveSet.get(end)] = this.aboveSet.get(begin);
                        this.vVertex[this.aboveSet.get(begin + 1)] = this.aboveSet.get(end);
                        this.wVertex[this.aboveSet.get(begin + 1)] = -1;
                        this.aboveSetEdges[numBlock][0] = this.aboveSet.get(begin);
                        this.aboveSetEdges[numBlock][1] = this.aboveSet.get(begin + 1);
                    }
                    else {
                        this.vVertex[this.aboveSet.get(begin + 1)] = this.aboveSet.get(begin);
                        this.wVertex[this.aboveSet.get(begin)] = -1;
                        this.vVertex[this.aboveSet.get(begin)] = this.aboveSet.get(end);
                        this.wVertex[this.aboveSet.get(begin)] = this.aboveSet.get(begin + 1);
                        this.vVertex[this.aboveSet.get(end)] = this.aboveSet.get(begin);
                        this.wVertex[this.aboveSet.get(end)] = -1;
                        this.aboveSetEdges[numBlock][0] = this.aboveSet.get(begin + 1);
                        this.aboveSetEdges[numBlock][1] = this.aboveSet.get(end);
                    }
                }
            }
        }
    }

    private void connectBlocks() {
        int size = this.aboveSetEdges.length;
        One2OnePath path = new One2OnePath();
        One2OnePath tempPath = new One2OnePath();
        double maxWeight = Integer.MIN_VALUE;
        double edgesToCompare[] = new double[4];
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1; j++) {
                edgesToCompare[0] = this.adjMatrix[this.aboveSetEdges[j][0]][this.aboveSetEdges[j + 1][0]];
                edgesToCompare[1] = this.adjMatrix[this.aboveSetEdges[j][0]][this.aboveSetEdges[j + 1][1]];
                edgesToCompare[2] = this.adjMatrix[this.aboveSetEdges[j][1]][this.aboveSetEdges[j + 1][0]];
                edgesToCompare[3] = this.adjMatrix[this.aboveSetEdges[j][1]][this.aboveSetEdges[j + 1][1]];
                tempPath = this.maxAmongFour(edgesToCompare, j);
               if (maxWeight > tempPath.getWeight()) {
                   path = tempPath;
               }
            }

            for (int h = 0; h < 4; h++) {
                edgesToCompare[0] = this.adjMatrix[this.aboveSetEdges[path.getStartFromLocalSet()][0]][this.aboveSetEdges[path.getStartFromLocalSet() + 1][0]];
                edgesToCompare[1] = this.adjMatrix[this.aboveSetEdges[path.getStartFromLocalSet()][0]][this.aboveSetEdges[path.getStartFromLocalSet() + 1][1]];
                edgesToCompare[2] = this.adjMatrix[this.aboveSetEdges[path.getStartFromLocalSet()][1]][this.aboveSetEdges[path.getStartFromLocalSet() + 1][0]];
                edgesToCompare[3] = this.adjMatrix[this.aboveSetEdges[path.getStartFromLocalSet()][1]][this.aboveSetEdges[path.getStartFromLocalSet() + 1][1]];
            }

            path = this.minPathAmongSets(edgesToCompare, path.getStartFromLocalSet());

            if (this.vVertex[this.aboveSet.get(path.getStart())] != -1) {
                this.wVertex[this.aboveSet.get(path.getStart())] = this.aboveSet.get(path.getEnd());
            }
            else {
                this.vVertex[this.aboveSet.get(path.getStart())] = this.aboveSet.get(path.getEnd());
            }

            if (this.vVertex[this.aboveSet.get(path.getStart())] != -1) {
                this.wVertex[this.aboveSet.get(path.getEnd())] = this.aboveSet.get(path.getStart());
            }
            else {
                this.wVertex[this.aboveSet.get(path.getEnd())] = this.aboveSet.get(path.getStart());
            }

            this.aboveSetEdges[path.getStartFromLocalSet()][path.getStart()] = -1;
            this.aboveSetEdges[path.getStartFromLocalSet() + 1][path.getEnd()] = -1;
        }
    }

    private One2OnePath maxAmongFour(double[] edgesWeight, int localSet)  {
        One2OnePath maxWeight = new One2OnePath();
        int v = 0;
        int w = 0;
        int max = 0;

        for (int i = 1; i < edgesWeight.length; i++)
        {
            if (edgesWeight[i] > edgesWeight[max] && edgesWeight[i] != -1 && edgesWeight[max] != -1) {
                max = i;
            }
        }

        if ( max > 1) {
            v = 1;
        }
        if (max % 2 != 0) {
            w = 1;
        }

        maxWeight.setStart(this.aboveSetEdges[localSet][v]);
        maxWeight.setEnd(this.aboveSetEdges[localSet + 1][w]);
        maxWeight.setStartFromLocalSet(localSet);
        maxWeight.setWeight(edgesWeight[max]);

        return maxWeight;
    }

    private One2OnePath minPathAmongSets(double[] edgesWeight, int localSet)  {
        One2OnePath minWeight = new One2OnePath();
        int v = 0;
        int w = 0;
        int min = 0;
        for (int i = 1; i < edgesWeight.length; i++ )
        {
            if (edgesWeight[i] > edgesWeight[min] && edgesWeight[i] != -1 && edgesWeight[min] != -1) {
                min = i;
            }
        }

        if ( min > 1) {
            v = 1;
        }
        if (min % 2 != 0) {
            w = 1;
        }

        minWeight.setStart(this.aboveSetEdges[localSet][v]);
        minWeight.setEnd(this.aboveSetEdges[localSet + 1][w]);
        minWeight.setStartFromLocalSet(localSet);
        minWeight.setWeight(edgesWeight[min]);

        return minWeight;
    }

    private int minAmongThree(double weight1, double weight2, double weight3)  {
        int minWeight;
        if(weight1 < weight2) {
            if (weight1 < weight3) {
                minWeight = 1;
            }
            else {
                minWeight = 3;
            }
        }
        else {
            if (weight2 < weight3) {
                minWeight = 2;
            }
            else {
                minWeight = 3;
            }
        }
        return minWeight;
    }

    private void sort(String set) {
        int n = 0;
        if(set.equals("above")) {
            n = this.aboveSet.size();
            this.forSorting = new double[n];
            for (int i = 0; i < n; i++) {
                int index = this.aboveSet.get(i);
                this.forSorting[i] = this.adjMatrix[0][index];
            }
        }
        else {
            n = this.underSet.size();
            this.forSorting = new double[n];
            for (int i = 0; i < n; i++) {
                int index = this.underSet.get(i);
                this.forSorting[i] = this.adjMatrix[0][index];
            }
        }
        for (int i = n / 2 - 1; i >= 0 ; i--) {
            sink(i, n - 1, set);
        }
        for (int i = n - 1; i > 0; i--) {
            exch(0, i, set);
            sink(0, i - 1, set);
        }
    }
    private void sink(int i, int j, String set) {
        int child;
        while (2*i < j) {
            child = 2*i + 1;
            if (child < j && (this.forSorting[child] < this.forSorting[child + 1])) {
                child++;
            }
            if (this.forSorting[i] > this.forSorting[child]) {
                break;
            }
            exch(i, child, set);
            i = child;
        }
    }
    private void exch(int i, int j, String set) {
        int tempi;
        double temp = this.forSorting[i];
        this.forSorting[i] = this.forSorting[j];
        this.forSorting[j] = temp;
        if(set.equals("above")) {
            tempi =  this.aboveSet.get(i);
            this.aboveSet.add(i, this.aboveSet.get(j));
            this.aboveSet.add(j, tempi);
        }
        else {
            tempi =  this.underSet.get(i);
            this.underSet.add(i, this.underSet.get(j));
            this.underSet.add(j, tempi);
        }
    }
}

package main.java;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Фирюза on 30.11.14.
 */
public class Program {
    public static void main(String[] args) throws IOException {
        //FileReader file = new FileReader("input.txt");
        //int n = file.read() - '0';
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int v;
        double x, y;
        OptimumPath optimumPath = new OptimumPath(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                v = scanner.nextInt();
                optimumPath.setValue(i, j, v);
            }
        }
        for (int i = 0; i < n; i++) {
            x = scanner.nextDouble();
            y = scanner.nextDouble();
            optimumPath.setCoordinates(i, x, y);
        }

        optimumPath.FindOptimumPath();
    }
}

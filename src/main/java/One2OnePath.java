package main.java;

/**
 * Created by Фирюза on 30.11.14.
 */
public class One2OnePath {

    private int startFromLocalSet;
    private int start;
    private int end;
    private double weight;

    public One2OnePath() {}

    public One2OnePath(int startFromLocalSet, int start, int end, double weight) {
        this.startFromLocalSet = startFromLocalSet;
        this.start = start;
        this.end = end;
        this.weight = weight;
    }

    public int getStartFromLocalSet() {
        return startFromLocalSet;
    }

    public void setStartFromLocalSet(int startFromLocalSet) {
        this.startFromLocalSet = startFromLocalSet;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
